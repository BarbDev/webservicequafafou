var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({message: 'Bienvenue sur lindex', success: true});
});

module.exports = router;
