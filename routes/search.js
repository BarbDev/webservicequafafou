let express = require('express');
let router = express.Router();
request = require('request-json');
// Pour rajouter paramètre : https://api.archives-ouvertes.fr/search/?q=(text:test%20authFirstName_sci:Nicolas)
let hal = request.createClient('https://api.archives-ouvertes.fr/search/');
let arXiv = request.createClient('https://export.arxiv.org/api/');
// 'https://export.arxiv.org/api/query?search_query=all:electron'
let convert = require('xml-js');
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let debug = require('debug')('routes:search');
let async = require("async");

//let conn = 'mongodb://root:toor123@ds023315.mlab.com:23315/quafaf';
let conn = 'mongodb://localhost:27017/quafaf'
mongoose.connect(conn, function (err) {
  if (err) {
    debug(err);
  } else
    debug('mongo est connecté');
});

let schemaLess = new Schema({}, {
  strict: false
});
let toMigrate = new Schema({
  id:  String,
  titre: String,
  description:   String,
  uri: String
});
let halDB = mongoose.model('hal', schemaLess);
let arXivDB = mongoose.model('arXiv', schemaLess);
let migrateDB = mongoose.model('toMigrate', toMigrate);

let halSearch = function(laRequete, cb) {
  hal.get(laRequete, function (err, response, body) {
    let maListe = [];
    if (body && body.response) {
      body.response.docs.forEach(element => {
        let elem = {
          id: element.docid,
          titre: element.title_s,
          description: element.label_s,
          uri: element.uri_s
        };
        maListe.push(elem);
        let toSave = new halDB(element);
        toSave.save();
        let migrate = new migrateDB(elem);
        migrate.save();
      });
      cb(null, maListe);
    } else {
      cb(null, maListe);
    }
  });
}

let arXivSearch = function(requete, cb) {
  arXiv.get(requete, function(err, response, body) {
    let maListe = [];
    if (!body) { cb(null, []); return; }
    let results = convert.xml2js(body, {compact: true, spaces: 4});
    if (!results) { cb(null, []); return; }
    results.feed.entry.forEach(element => {
      let elem = {
        id: element["arxiv:doi"]._text,
        titre: element.title._text,
        description: element.summary._text,
        uri: element.id._text
      };
      maListe.push(elem);
      let toSave = new arXivDB(element);
      toSave.save();
      let migrate = new migrateDB(elem);
      migrate.save();
    });
    cb(null, maListe);
  });
}

router.post('/', function (req, res, next) {
  let search = req.body.search;
  debug('SEARCH --> ' + search);
  if (search == null) {
    res.send({
      success: false,
      error: 'Pas de recherche fait'
    });
  } else {
    laRequete = '?q=' + search + '&fl=title_s, label_s, docid, uri_s';
    funcs = [];

    funcs.push(async.apply(halSearch, laRequete));
    funcs.push(async.apply(arXivSearch, 'query?search_query=all:electron'));
    async.parallel(funcs, function(err, results) {
        if (err) res.send({success: false});
        if (results) {
          if (results.length > 1) {
            results = results[0].concat(results[1]);
          }
        }
        res.send({
          success: true,
          results: results
        });
    });
  }
});


module.exports = router;