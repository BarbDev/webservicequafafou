var express = require('express');
var router = express.Router();
request = require('request-json');
var client = request.createClient('https://api.archives-ouvertes.fr/search/');


// var lié à BD mongodb
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// connection à la BD
mongoose.connect('mongodb://localhost/Search', function(err) {
    if(err) {throw err;}
    else
        console.log('mongo est connecté');
});

/// declare schema user
var UserSchema = Schema({
  id: String,
  titre: String,
  description: String,
  uri: String
});

// Itialisation model
var model = mongoose.model('laboratorySearch', UserSchema);

/**
 * Permet à l'utilisateur
 */
router.post('/', function (req, res, next) {
  let search = req.body.search;
  if (search == null) {
    res.send({
      success: false,
      error: 'Pas de recherche fait'
    });
  } else {
    laRequete = '?q=labStructAcronym_t:' + search + '&fl=title_s, label_s, docid, uri_s';

    // Pour rajouter paramètre : https://api.archives-ouvertes.fr/search/?q=(text:test%20authFirstName_sci:Nicolas)

    // https://api.archives-ouvertes.fr/search/?q=authFullName_s:*durant*&fl=authFullName_s

    // https://api.archives-ouvertes.fr/search/?q=labStructAcronym_t:CNRS

    client.get(laRequete, function (err, response, body) {
      let maListe = [];
      body.response.docs.forEach(element => {
        let elem = {
          id: element.docid,
          titre: element.title_s,
          description: element.label_s,
          uri: element.uri_s
        };
        maListe.push(elem);
      });

      /*var ObjectToSave = new model({
          id: element.docid,
          titre: element.title_s,
          description: element.label_s,
          uri: element.uri_s
        });


        ObjectToSave.save(function(err) {
          if(err) {
              throw err;
          }
          else {
            
          }
        });*/

      res.send({
        success: true,
        results: maListe
      });
    });
  }
});

module.exports = router;