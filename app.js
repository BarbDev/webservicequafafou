var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var utf8 = require('utf8');

var indexRouter = require('./routes/index');
var indexSearch = require('./routes/search');
var indexSearch2 = require('./routes/search2');
var indexSearch3 = require('./routes/search3');
var indexSearch4 = require('./routes/search4');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/search', indexSearch);
app.use('/search2', indexSearch2);
app.use('/search3', indexSearch3);
app.use('/search4', indexSearch4);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({error: err.message, success: false});
});

module.exports = app;
