var routingApp = angular.module('routeApp', ['ui.router', 'blockUI']);

routingApp.config(function ($stateProvider, $urlRouterProvider, blockUIConfig) {
	blockUIConfig.message = 'Patientez SVP...';
	
	var searchState = {
		name: 'search',
		url: '/',
		templateUrl: 'vue_search.html',
		controller: 'searchCtrl'
	};

	$stateProvider.state(searchState);

	$urlRouterProvider.otherwise('/');
});
