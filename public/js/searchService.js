routingApp.factory('searchService', ['$http', '$state', function($http, $state) {

    var serv = {};

    serv.search = function(search, cb) {
        var req = {
            search: search
        };
        console.log('search');
        $http.post('/search', req).then(function (resp) {
            console.log('Service');
            console.log(resp.data.results)
            cb(resp.data.results);
        });
    },

    serv.searchLaboratoire = function(search, cb) {
        var req = {
            search: search
        };

        $http.post('/search2', req).then(function (resp) {
            console.log('ServiceLaboratoire');           
            cb(resp.data.results);
        });
    }

    serv.searchUniversite = function(search, cb) {
        var req = {
            search: search
        };

        $http.post('/search3', req).then(function (resp) {
            console.log('ServiceUniversite'); 
            console.log(resp.data.results);          
            cb(resp.data.results);
        });
    }


    serv.searchCoAuthor = function(search, cb) {
        var req = {
            search: search
        };

        $http.post('/search4', req).then(function (resp) {
            console.log('searchCoAuthor'); 
            console.log(resp.data.results);          
            cb(resp.data.results);
        });
    }

    return serv;
}]);
