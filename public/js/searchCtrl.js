routingApp.controller('searchCtrl', ['$scope', '$sce', '$http', '$location', 'searchService', function ($scope, $sce, $http, $location, searchService) {

    angular.element(document).ready(function () {
        let elems = document.querySelectorAll('.modal');
        let options = null;
        let instances = M.Modal.init(elems, options);
    });

    $scope.model3 = false;
    $scope.search = null;
    $scope.results = [];
    $scope.resultLaboratoire = [];
    $scope.resultUniversite = [];
    $scope.resultCoAuteurs = [];

    $scope.viderListe = function() {
        $scope.results = null;
        $scope.results = [];
    };

    $scope.launchSearch = function() {

        console.log("bool");
        console.log($scope.search);
        if($scope.search == null) {
            $scope.viderListe();
            console.log("VIDEEEEEEEE");
        }
        else if(!$scope.model1 & !$scope.model2 & !$scope.model3) {    
            searchService.search($scope.search, function(results) {
                $scope.results = results;
                console.log('Ctrl');
                console.log(results);
            });
        }
        else if($scope.model1 & !$scope.model2 & !$scope.model3) {    
            searchService.searchLaboratoire($scope.search, function(results) {
                $scope.results = results;
                console.log('Ctrl');
                console.log(results);
            });
        }
        else if(!$scope.model1 & $scope.model2 & !$scope.model3) {
            searchService.searchUniversite($scope.search, function(results) {
                $scope.results = results;
                console.log('Ctrl');
                console.log(results);
            });
        }
        else if(!$scope.model1 & !$scope.model2 & $scope.model3) {
            searchService.searchCoAuthor($scope.search, function(results) {
                $scope.results = results;
                console.log('Ctrl');
                console.log(results);
            });
        }
        else {
            $scope.viderListe();
        }
    };
}]);
